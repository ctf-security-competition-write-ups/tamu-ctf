# TAMU CTF 2019

Welcome  to the Annual Texas A&M Cyber Security Center Capture the Flag Contest

We placed 112th place. 

I have completed write ups for those challenges we have completed.  Except 
for one challenge, that was totally above my head, in the binary exploitation 
department.

Note*** I did not do write ups on the honey pot challenges, or the general 
network traffic analyses challenges.  As the time required to go through all the
log files again, for the purposes of a write up would be very demanding. 
We did however complete all the network traffic analyses challenges, and all but
one honey pot challenge.